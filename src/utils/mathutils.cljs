;; Some general math utility functions for a variety of purposes
;; Karsten Schmidt's thi.ng library is a dependency
;; http://thi.ng/

(ns utils.mathutils
  (:require
    [thi.ng.typedarrays.core :as tarrays]
    [thi.ng.math.core :as math]
    [thi.ng.geom.core.vector :as v :refer [vec2 vec3]]))

(defn constrain [amt low hight]
  "Constrains a value between two values"
  (if (< amt low)
    (low)
    (if (> amt hight)
      (hight)
      (amt))))

(defn flattenArray [vecarray]
    "Takes a array of vectors and flatens things so that all of the values
    flow consecutively one after another, ie [x,y,z,x,y,z]. Assumes that your vector
    object has x y and z properties on it"
    (let [finalArray #js []]
      (dotimes [i (.-length vecarray)]
        (let [vec (nth vecarray i)]
          (if (= (type vec) js/THREE.Vector3)
            (let [reserve 0]
              (.push finalArray (.-x vec))
              (.push finalArray (.-y vec))
              (.push finalArray (.-z vec)))
              (.push finalArray vec))
            ))
finalArray))

(defn float32 [amountOrArray]
  "Builds a float32 array. Ensures that the array is in a flat state consisting of
  just values as opposed to a arrray of objects, then returns the new array"
  (if (= (type amountOrArray) js/Array)
    (let [flatArray (flattenArray amountOrArray)]
      (tarrays/float32 flatArray))
    (tarrays/float32 (* 3 amountOrArray))))

(defn toThingVector [threeVector]
  "Converts a Three.js vector to a Thi.ng vector"
  (vec3 (.-x threeVector) (.-y threeVector) (.-z threeVector) ))

(defn assignRandToArray [arr value]
  "Enables you to quickly populate a array with a random value.
  returns the newly populated array "
  (loop [i 0]
    (if (< i (.-length arr))
      (let [x i
            y (+ 1 i)
            z (+ 2 i)]
          (aset arr x (* js/Math.random value))
          (aset arr y (* js/Math.random value))
          (aset arr z (* js/Math.random value))
        (recur (+ 3 i)))
      ()))
      arr)


(defn assignToArray [arr cb]
  "This function loops through a flat array and allows you to set values in sets of 3 in a callback"
  (loop [i 0]
    (if (< i (.-length arr))
      (let [x i
            y (+ 1 i)
            z (+ 2 i)]
          (cb arr x y z)
        (recur (+ 3 i)))
      ()))
      arr)
