(ns utils.shader
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
    [goog.dom :as dom]
    [goog.events :as events]
    [utils.glutils :as utils]
    [ajax.core :refer [GET POST]]
    [thi.ng.typedarrays.core :as tarrays]
    [cljs.core.async :refer [put! chan <!]]))


  ;;define base locations for fragment and vertex shaders
  (def vertexFolder "shaders/vertex")
  (def fragmentFolder "shaders/fragment")

  (defn loadShaderCode [path]
    "core.async handler to load the code"
    (let [code (chan)]
          (GET path {
            :handler (fn [response]
              (put! code response))
            })
         code))

  (defn init[path]
    "Initiates the loading for both the vertex and fragment shaders and passes the results back in a
    single map"
    (go
      (let [
          fragment (<! (loadShaderCode (str fragmentFolder "/" path)))
          vertex (<! (loadShaderCode (str vertexFolder "/" path)))
          ]
      {
        :vertexShader vertex
        :fragmentShader fragment
        })))


  (defn loadShader [path callback]
    "This loads a set of shaders for use in a WebGL program. Will return the set in a PersistantVector
    the first index holds the vertex shader, the last index holds the fragment shader"
    (go (let [result (<! (init (str path ".glsl")))]
      (callback result))
      ))
