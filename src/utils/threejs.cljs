;; a set of wrappers for working with Three.js
(ns utils.threejs
  (:require
   [thi.ng.math.core :as math]
   [thi.ng.typedarrays.core :as tarrays]))


(defn buildAttribute
  [name num]
  (let [arr (tarrays/float32 (* 3 num))]
    (let [attribute (js/THREE.BufferAttribute. arr 3)]
      (set! (.-name attribute) name)
      attribute
      )))


(defn buildAttributeFromArray [name float32array]
  (if (= (type float32array) js/Float32Array)
    (let [attribute (js/THREE.BufferAttribute. float32array 3)]
      (set! (.-name attribute) name)
      attribute)))


(defn getAspectRatio
  ([](/ js/window.innerWidth js/window.innerHeight))
  ([el]
   (let [styles (js/window.getComputedStyle el)]
     (let [width (js/parseInt (.-width styles))
           height (js/parseInt (.-height styles))]
        (/ width height)))))
