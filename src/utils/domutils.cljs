;; a series of utility functions taken from various parts of the interwebs
;; so far
;; 1. Dommy (https://github.com/Prismatic/dommy)
(ns utils.domutils)


(defn as-str
  "Coerces strings and keywords to strings, while preserving namespace of
   namespaced keywords"
  [s]
  (if (keyword? s)
    (str (some-> (namespace s) (str "/")) (name s))
    s))

;; sets teh style of a element
(defn set-style
  "Set the style of `elem` using key-value pairs:
      (set-style! elem :display \"block\" :color \"red\")"
  [elem & kvs]
  (assert (even? (count kvs)))
  (let [style (.-style elem)]
    (doseq [[k v] (partition 2 kvs)]
      (.setProperty style (as-str k) v))
    elem))

;; Not normally needed but needed cause tracking js for some reason just wants regular arrays and reconverts
(defn typedToRegularArray [typedArray]
  (.-prototype.-slice.call typedArray))


;;example of default params
;;(defn testDefault [x & {:keys [a] :or {a 999}}]
  ;;  (js/console.log (str "a is " a )))
