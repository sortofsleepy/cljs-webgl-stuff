
(ns utils.glutils)

(defn usingThreeJS []
  "A conditional function so we can check to see if we're using Three.js or not"
  (if (not= js/undefined js/window.THREE)
    (let [usingThree js/true]
      usingThree)
    (let [usingThree js/false]
    usingThree)))

(defn getDefaultUniforms[]
  "Returns some default uniforms that could be used in a variety of contexts"
    (clj->js {
      :time (clj->js {
        :type "f"
        :value 0
        })
        :originTexture (clj->js {
          :type "t"
          :value nil
          })

        :destinationTexture (clj->js {
          :type "t"
          :value nil
          })
      }))

(defn generatePassthru[uniforms]
  "This creates a pass-thru shader program. It will default to
  a THREE.ShaderMaterial if Three.js is available, otherwise it'll attempt
  to build a program out of raw WebGL"
  (if (not= js/undefined (js/window.THREE))
    (let [ vertex (str
            "varying vec2 vUv;
            void main() {
              vUv = uv;
              gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
            }")

            fragment (str "uniform sampler2D texture;
            varying vec2 vUv;
            void main() {
              vec2 read = vUv;
              vec4 c = texture2D( texture , vUv );
              gl_FragColor = c ;
               //gl_FragColor = vec4(1.);
            }")

            ]
      (if (= uniforms js/undefined)
        (let [uniforms (clj->js {:texture (clj->js {:type "t" :value "null"})})]
          (let [shaderMaterial (js/THREE.ShaderMaterial (clj->js {
            :vertexShader vertex
            :fragmentShader fragment
            :uniforms uniforms
            }))]
            shaderMaterial))))
    ;; if we're using regular gl
    ()
    ))

(defn getPassthruVertex []
  (let [usingTHREE (usingThreeJS)]
    (if usingTHREE
      (str
              "varying vec2 vUv;
              void main() {
                vUv = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
              }")
      (js/console.log "NOT USING THREE - need to generate regular passthrue"))
      ))


(defn createShader [x & {:keys [vertex fragment uniforms attributes] :or {vertex (getPassthruVertex) fragment "default-f.glsl" uniforms nil attributes nil}}]
  "This creates a shader program. If we're using Three.js, it'll return a THREE.ShaderMaterial, if not, we return a
  regular webgl ShaderProgram"
  (let [usingThree (usingThreeJS)]
    (if (= js/true usingThree)
      (let [vertexShader vertex]
        (if (= uniforms nil)
          (let [uniforms (getDefaultUniforms)]
            (js/THREE.ShaderMaterial. (clj->js {
              :vertexShader vertexShader
              :fragmentShader fragment
              :uniforms uniforms
              })))))
      (js/console.log "TODO : generate shader for regular gl")

      )))


  (defn generateRenderTarget [ width height]
    "Generates and returns a render target or fbo depending on the context we're using"
    (let [usingThree (usingThreeJS)]
      (if (= js/true usingThree)
        (js/THREE.WebGLRenderTarget. width height (clj->js {
          :minFilter (aget js/THREE "NearestFilter")
          :magFilter (aget js/THREE "NearestFilter")
          :width width
          :height height
          :format (aget js/THREE "RGBAFormat")
          :type (aget js/THREE "FloatType")
          :stencilBuffer js/false
          })))))
