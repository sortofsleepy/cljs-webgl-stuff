(ns audio.audioanalyser
  (:require
   [thi.ng.typedarrays.core :as tarrays]))


(deftype AudioAnalyser [sourceurl]
  Object
  (build [this]
         (let [audio (js/Audio.)]
           (aset audio "src" sourceurl)
           (.appendChild js/document.body audio)
           (let [analyser (js/WebAudioAnalyser. audio)]
             (aset this "audio" audio)
             (aset this "avgLevel" 0)
             (aset this "volumeSensitivity" 1)
             (aset this "binCount" (.-frequencyBinCount analyser))
             (aset this "levelsCount" 16)
             (aset this "levelBins" (js/Math.floor (/ (.-binCount this) (.-levelsCount this))))
             (aset this "width" (/ 4 (.-frequencyBinCount analyser)))
             (aset this "levelsCount" 16)

             (aset this "levelsData" #js [])
             (aset this "freqByteData" (tarrays/uint8 (.-binCount this)))
             (aset this "timeByteData" (tarrays/uint8 (.-binCount this)))
             (aset this "dataWaveForm" #js [])
             (aset this "levelsHistory" #js [])

             (aset this "BEAT_HOLD_TIME" 40)
             (aset this "BEAT_DECAY_RATE" 0.98)
             (aset this "BEAT_MIN" 0.15)

             (aset this "count" 0)
             (aset this "msecsFirst" 0)
             (aset this "msecsPrevious" 0)
             (aset this "msecsAvg" 633)
             (aset this "gotBeat" false)
             (aset this "beatCutOff" 0)
             (aset this "beatTime" 0)
             (aset this "beatHoldTime" 40)
             (aset this "beatDecayRate" 0.97)
             (aset this "bpmStart" 0)

             (aset this "isPlaying" false)

             ;; apply audio analyser props onto this Object
             (.extend js/_ this analyser)
           ))
         this)

   (update [this]
           (if (= true (.-isPlaying this))
             (let [timeByteData (.-timeByteData this)
                   freqByteData (.-freqByteData this)]
               (.waveform this timeByteData)
               (.frequencies  this freqByteData)
               (.updateWaveform this)
               (.normalizeLevels this)
             ())))

  (getLevelsArray [this]
                  (.-levelsData this))

  (getChannelData [this index]
                  (nth (.-levelsData this) index))

  (getFrequencyData [this index]
                     (nth (.-freqByteData this) index))


  (getWaveformData [this index]
                   (nth (.-dataWaveform this) index))

  (getLevelHistory [this index]
                   (nth (.-levelHistory this) index))

  (updateWaveform [this]())
  (normalizeLevels [this]())


  )


  ;; creates and returns a AudioAnalyser
  (defn makeAnalyser [sourceurl]
    (let [analyser (AudioAnalyser. sourceurl)]
      (.build analyser)))


