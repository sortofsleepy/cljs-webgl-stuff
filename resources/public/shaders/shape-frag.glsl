#define GLSLIFY 1
varying vec3 vNormal;
varying vec3 vColor;
varying vec2 vUv;
uniform float Time;
uniform sampler2D AudioTexture;
uniform vec2 resolution;

highp float random_1_0(vec2 co)
{
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt= dot(co.xy ,vec2(a,b));
    highp float sn= mod(dt,3.14);
    return fract(sin(sn) * c);
}



void main() {

	const float ambient = 0.4;

	   vec2 uv = gl_FragCoord.xy / resolution.xy;

    vec4 tex = texture2D(AudioTexture,gl_PointCoord);
   vec4 f = vec4(0.);


       vec2 u = vUv;

         f.xy = .5 - u;

         float t = Time,
               z = atan(f.y,tex.y) * 2.,
               v = cos(z + sin(t * .1)) + .5 + sin(tex.x * 10. + (t * tex.y) * 1.3) * .4;

         f.x = 1.2 + cos(z - t *.2) + sin(u.y * 30.+ t * 1.5) * .5;
     	f.yz = vec2( sin(v*4.)*.25, sin(v*2.)*.3 ) + f.x*.5;

     	f *= mix(f,tex,0.2);

       gl_FragColor = f;

}
