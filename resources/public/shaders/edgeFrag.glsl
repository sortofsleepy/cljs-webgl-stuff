#define GLSLIFY 1
uniform float channelSize;
uniform sampler2D AudioTexture;
varying vec2 vUv;
uniform float Time;



void main(){
    vec2 uv = vUv;
    vec4 audio = texture2D(AudioTexture,gl_PointCoord);
    vec4 f = vec4(0.);

    vec2 u = vUv;

      f.xy = .5 - u;

      float t = Time,
            z = atan(f.y,f.x) * 2.,
            v = cos(z + sin(t * .1)) + .5 + sin(u.x * 10. + t * 1.3) * .4;

      f.x = 1.2 + cos(z - t*.2) + sin(u.y*30.+t*1.5)*.5;
  	f.yz = vec2( sin(v*4.)*.25, sin(v*2.)*.3 ) + f.x*.5;

    gl_FragColor = audio * f;
}